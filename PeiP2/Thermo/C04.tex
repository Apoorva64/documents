\input{preamble}
\usepackage{tikzit}
\input{figures/styles.tikzstyles}

\title{ C4 - 2e principe de la thermodynamique }

\begin{document}
	%\includecourse

	\maketitle

	\section{Entropie}%
	
	\ctikzfig{c4-1-1}

	Égalité de Clausius :
	\begin{align*}
		\oint_{cycle} \frac{\delta Q_{rev}}{T}=0
	\end{align*}

	Valable pour tout cycle réversible et pour tout type de matière avec 
	\begin{itemize}
		\item $\delta Q_{rev}$ : chaleur reçue au cours d'une transformation
			infinitésimale $i-k$

		\item $T$ : température au point $i$
	\end{itemize}
	
	Vérification pour un gaz parfait :

	1er principe pour une transformation infinitésimale et réversible :
	\begin{align*}
		&dU= \delta W_{ext\to gaz}+\delta Q_{ext\to gaz} \\
		\donc& \nu c_v dT = -PdV+\delta Q_{rev}
		\shortintertext{$PV=\nu RT$ :}
		\donc& \nu c_v \frac{dT}{T}=- \nu R \frac{dV}{V}+\frac{\delta Q_{rev}}{T}\\
		\donc& \oint_{cycle} \nu c_v \frac{dT}{T}=- \oint_{cycle} \nu R \frac{dV}{V}+
		\oint_{cycle} \frac{\delta Q_{rev}}{T}\\
		\donc& \underbrace{\nu c_v \int_{T_1}^{T_1} \frac{dT}{T}  }_{=0} = \underbrace{-\nu R
		\int_{v_1}^{v_1} \frac{dv}{v}}_{=0} + \oint_{cycle} \frac{\delta Q_{rev}}{T} \\
		\donc& \oint_{cycle} \frac{\delta Q_{rev}}{T}=0
	\end{align*}

	Soit $\frac{\delta Q_{rev}}{T}=dS$ 

	$\oint_{cycle} \frac{\delta Q_{rev}}{T}=\oint_{cycle} dS=0$ d'où $\Delta
	S_{cycle}=0$ 

	$S$ est une fonction d'état : 
	\begin{align*}
		S= \int \frac{\delta Q_{rev}}{T} + c
	\end{align*}
	Elle est appelée entropie.

	Variation de l'entropie au cours d'une transformation réversible.
	\begin{align*}
		\Delta S = S_2-S_1 = \int_{1}^{2} \frac{\delta Q_{rev}}{T} 
	\end{align*}
	Unité SI : $[S] = \frac{[Q]}{[T]}=\frac{J}{K}$ 

	\begin{expl}
		\begin{enumerate}
			\item Evaporation de l'eau

			Ordre / Désordre : 

			Liquide : réseau cristallin "tordu". État plus ordonné

			Vapeur : aucune organisation spatiale. État plus désordonné

			Variation de l'entropie :
			\begin{align*}
				\Delta S = \int_{1}^{2} \frac{\delta
				Q_{rev}}{\underbrace{T}_{=\SI{373.15}{\kelvin}}}=\underbrace{\frac{Q_{\text{evaporation}}}{T}}_{>0} 
			\end{align*}
			Donc $S_{fin}>S_{ini}$ 

			\item Solidification de l'eau

			Ordre / Désordre :

			Liquide : réseau cristallin "tordu". État plus désordonné

			Solide : réseau cristallin. État plus ordonné

			Variation de l'entropie :
			\begin{align*}
				\Delta S &= \int_{1}^{2} \frac{\delta
				Q_{rev}}{\underbrace{T}_{\SI{273.15}{\kelvin}}} =
				\underbrace{\frac{Q_{solidification}}{T}}_{Q<0}
			\end{align*}
			Donc $S_{fin}<S_{ini}$

		\end{enumerate}
	\end{expl}

	\begin{defi}
		L'entropie mesure le degré de désordre d'un système. Plus l'entropie est élevée,
		moins les éléments du système sont ordonnés, liés entre eux.
	\end{defi}

	\begin{fml}
		\begin{align*}
			S= \int \frac{\delta Q_{rev}}{T} + c
		\end{align*}
	\end{fml}

	\section{Transformations réversibles}%
	\label{sec:transformations_reversibles}
	
	\subsection{Définition}%
	\label{sub:definition}
	
	\begin{defi}
		La transformation thermodynamique est dite réversible lorsque le chemin de la
		transformation inverse coincide avec le chemin de la transformation directe.

		La transformation inverse ramène ainsi le système dans son état initial.

		La transformation réversible n'est possible qu'en absence de frottements dans le
		système.
	\end{defi}

	\ctikzfig{c4-2-1}

	\subsection{Diagramme TS}%

	\begin{center}
		\tikzfig{c4-2-2}
		\tikzfig{c4-2-3}
	\end{center}

	\begin{itemize}
		\item 
		On étudie la transformation infinitésimale réversible $i-k$. Variation de l'entropie
		$dS = \frac{\delta Q_{rev}}{T}$ 

		Chaleur élémentaire reçue : $\delta Q_{rev}=T dS$ 

		Sur le diagramme TS : $\abs{ \delta Q_{rev} }=Aire[CikD]$ 

		\item On étudie la transformation réversible 1-2

		Chaleur reçue : $Q_{rev}=\int_{1}^{2} \delta Q_{rev}=\int_{S_1}^{S_2} T( S )dS$ 

		Sur $TS$ : $\abs{ Q_{rev} }=Aire[A12B]$
	\end{itemize}
	La valeur absolue de la chaleur reçue représente l'aire au dessous de la courbe $1-2$ 
	sur le diagramme TS.

	\subsection{1er principe de la thermodynamique pour des transformations réversibles}%
	
	Pour une transformation infinitésimale réversible :
	\begin{align*}
		\left\{
		\begin{NiceMatrix}
			dU = \delta W_{ext\to sys}+\delta Q_{ext\to sys}\\
			\delta W_{ext\to sys}=-PdV \\
			\delta Q_{ext\to sys}=TdS
		\end{NiceMatrix}
		\right. \implies dU = -PdV+TdS
	\end{align*}
	D'où :
	\begin{fml}
		1er principe pour transformations réversibles :
		\begin{align*}
			TdS = dU + PdV
		\end{align*}
	\end{fml}
	\begin{rmq}
		\begin{align*}
			\delta Q_{rev}&=TdS\\
			\delta Q_{irrev} &\neq TdS
		\end{align*}
	\end{rmq}

	\subsection{Variation de l'entropie des gaz parfaits, liquides, solides incompressibles}%
	
	\ctikzfig{c4-2-4}
	
	On cherche 
	\begin{align*}
		\Delta S &= S_2-S_1 \\
				 &= f_1( T_1,T_2, V_1,V_2 ) \\
				 &= f_2( T_1,T_2,P_1,p_2 )
	\end{align*}

	\textbf{Cherchons $f_2$}

	On utilise le 1er principe : 
	\begin{align}
		TdS &= dU + PdV\\
		dU &= \nu c_v dT\\
		PV &= \nu RT \implies P = \frac{\nu RT}{V}
	\end{align}

	D'où :
	\begin{align*}
		dS &= \nu c_v \frac{dT}{T}+\nu R \frac{dV}{V} \\
		\Delta S &= \int_{S_1}^{S_2} dS  \\
		&= \int_{T_1}^{T_2} \nu c_v \frac{dT}{T}+ \int_{V_1}^{V_2} \nu R \frac{dV}{V}   \\
		&= \nu c_v \frac{T_2}{T_1}+\nu R \ln\left(\frac{V_2}{V_1}\right)
	\end{align*}

	\textbf{Cherchons $f_1$}

	On exprime $PdV$ en fonction de dP :
	\begin{align}
		d( PV )=PdV+VdP \implies PdV=d( PV )-VdP
	\end{align}
	On déduit de $( 1 ), ( 4 )$ :
	\begin{align*}
		TdS&= \underbrace{dU+d( PV )}_{d( U+PV )}-VdP \numberthis \\
		dH &= \nu c_p dT  \numberthis\\
		V &= \frac{\nu RT}{P} \numberthis \\
		\shortintertext{On déduit de $( 6 ),( 7 ),( 5 )$ : }
		TdS &= \nu c_p dT - \frac{\nu RT}{P}dP\\
		\implies dS &= \nu c_p \frac{dT}{T}-\frac{\nu R}{P}dP \\
		\implies \Delta S &= \int_{T_1}^{T_2} \nu c_p \frac{dT}{T}- \int_{P_1}^{P_2} \nu R
		\frac{dP}{P}\\
		&= \nu c_p \ln\left(\frac{T_2}{T_1}\right) -\nu R \ln\left(\frac{P_2}{P_1}\right)
	\end{align*}

	\begin{fml}
		\textbf{Gaz parfait :} 
		\begin{align*}
			\Delta S =
			\left\{
			\begin{NiceMatrix}
				\nu c_v \ln\left(\frac{T_2}{T_1}\right) + \nu R
				\ln\left(\frac{V_2}{V_1}\right)\\
				\nu c_v \ln\left(\frac{T_2}{T_1}\right) - \nu R
				\ln\left(\frac{P_2}{P_1}\right)
			\end{NiceMatrix}
			\right.
		\end{align*}
	\end{fml}

	Pour liquide, solide incompressible :
	\begin{align*}
		( 1 )\implies TdS &= dU + P \underbrace{dV}_{\simeq 0} \simeq dU = \nu c_v dT = M
		c_m dT\\
		\implies dS &= M c_m \frac{dT}{T}\\
		\implies \Delta S &= \int_{T_1}^{T_2} Mc_m \frac{dT}{T}\\
						  &= M c_m \ln\left(\frac{T_2}{T_1}\right)
	\end{align*}
	\begin{fml}
		\textbf{Solide, liquide incompressible :} 
		\begin{align*}
			M c_m \ln\left(\frac{T_2}{T_1}\right)
		\end{align*}
		Avec $M$ masse, $c_m$ capacité thermique massique en
		$\SI{}{\joule\per\kelvin\per\kilo\gram}$
	\end{fml}

	\section{Transformations irréversibles, systèmes isolés}%
	
	\subsection{Classification des systèmes}%
	
	Il y a trois types de système :
	\begin{itemize}
		\item Système isolé : aucun échange, $Q=W=0$ 
		\item Système thermiquement isolé : $Q=0, W\neq 0$. Exemple : piston
		\item Système non isolé : $Q\neq 0,W\neq 0$
	\end{itemize}

	\subsection{Mélange de deux gaz parfaits}%
	
	Transformation directe. On a un système contenant deux sous-systèmes, séparés par une
	paroi :
	\begin{itemize}
		\item $O_2$ : $\nu_1, P_1,V_1,T_1$
		\item $N_2$ : $\nu_1, P_1,V_1,T_1$
	\end{itemize}

	La paroi rompt. On obtient un système final avec $\nu_1 + \nu_2, P_f, 2V, T_f$ 

	On a $Q=0, W=0$. C'est donc un système isolé.

	On a : concentration $N_2, O_2$ hétérogène $\to$ concentration $N_2,O_2$ homogène

	état hors équilibre $\to$ état d'équilibre

	La transformation inverse, à l'issue de laquelle toutes les molécules $O_2$ se
	déplaceraient vers la gauche et toutes les molécules $N_2$ se déplaceraient vers la
	droite est impossible sans qu'on fournisse du travail ou de la chaleur au système.

	Le système ne reviendra jamais spontanément dans son état initial.

	\textbf{Retour forcé à l'état initial} 

	état final $\to $ liquéfaction $N_2+O_2, Q\neq 0$ $\to $ centrifugation + remise cloison
	$W\neq 0$ $\to $ évaporation $N_2$ et $O_2$ $\to $ état initial
 
	Donc, retour par un autre chemin que le chemin d'aller

	\begin{defi}
		Une transformation thermodynamique est appelée irréversible si le système ne peut
		pas revenir à son état initial par le même chemin. Le chemin de la transformation
		inverse ne coincide pas avec le chemin de la transformation directe.
	\end{defi}

	\textbf{Étude de la transformation directe (mélange)} 

	1) 1er principe 
	\begin{align*}
		\Delta U &= W_{ext\to sys}+Q_{ext\to sys}=0\\
		\Delta U &= \Delta U_{O_2}+\Delta U_{N_2} = \underbrace{\nu c_v( T_f-T_i )}_{O_2}
		+ \nu c_v ( T_f-T_i )=0\\
		\implies T_f = T_i
	\end{align*}

	2) Variation de l'entropie de $O_2+N_2$ 
	\begin{align*}
		\Delta S &= \Delta S_{O_2} + \Delta S_{N_2}\\
				 &= \nu c_v \ln\left(\frac{T_f}{T_i}\right)+\nu R
				 \ln\left(\frac{V_f}{V_i}\right) + \nu c_v
					 \ln\left(\frac{T_f}{T_i}\right) + \nu
					 \ln\left(\frac{V_f}{V_i}\right)\\
				 &= 2 \nu R \ln(2) > 0
	\end{align*}

	Lors du mélange des deux gaz, le système passe d'un état ordonné vers un état
	désordonné. L'entropie du système augmente.
\end{document}
